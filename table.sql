-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 26, 2016 at 06:55 PM
-- Server version: 5.5.34
-- PHP Version: 5.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `form`
--

-- --------------------------------------------------------

--
-- Table structure for table `table`
--

CREATE TABLE IF NOT EXISTS `table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `father` varchar(50) NOT NULL,
  `mother` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `number` varchar(30) NOT NULL,
  `address` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `table`
--

INSERT INTO `table` (`id`, `name`, `father`, `mother`, `email`, `number`, `address`) VALUES
(1, 'ziaul', 'halim', 'rizi', 'talukderziaul2@gmail.com', '01728388751', 'West Shewrapara, Mirpur dhaka*1216'),
(2, 'ziaul', 'Abdul Halim talukder', 'Rizi begum', 'mohammadziaulm62@gmail.com', '01728388751', 'sdfasdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdf'),
(3, 'ziaul', 'halim', 'Rizi begum', 'mohammadziaulm62@gmail.com', '01728388751', 'kasdkhakdhkadkadkakdhakhd'),
(4, 'ziaul', 'halim', 'Rizi begum', 'mohammadziaulm62@gmail.com', '01728388751', 'kasdkhakdhkadkadkakdhakhd'),
(5, 'Islamik History', 'Abdul Halim talukder', 'Rizi begum', 'zafarnub92@gmail.com', '01728388751', 'asdfsfdxvxvxvxccvxvxv'),
(6, 'Islamik History', 'Abdul Halim talukder', 'Rizi begum', 'mohammadziaulm62@gmail.com', '01728388751', 'ccccccccccccccccccccccccccccccccccccccccc'),
(7, 'Islamik History', 'Abdul Halim talukder', 'Rizi begum', 'mohammadziaulm62@gmail.com', '01728388751', 'ccccccccccccccccccccccccccccc'),
(8, 'Islamik History', 'Abdul Halim talukder', 'Rizi begum', 'mohammadziaulm62@gmail.com', '01728388751', 'dddddddddddddddddd'),
(9, 'Islamik History', '', '', '', '', ''),
(10, 'Islamik History', '', '', '', '', ''),
(11, 'Islamik History', 'Abdul Halim talukder', 'Rizi begum', 'mohammadziaulm62@gmail.com', '01728388751', '111111111111111111111111111111111111111111111111111111'),
(12, 'Islamik History', 'Abdul Halim talukder', 'Rizi begum', 'mohammadziaulm62@gmail.com', '01728388751', 'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz'),
(13, 'Islamik History', 'Abdul Halim talukder', 'Rizi begum', 'mohammadziaulm62@gmail.com', '01728388751', 'sdfaaaaaaaaaaaaaa');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
