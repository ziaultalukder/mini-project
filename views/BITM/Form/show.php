<?php
include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."registration_form".DIRECTORY_SEPARATOR."vendor/autoload.php");
 
use \App\BITM\SEIP108222\Form\Form;

$form = new Form();

$frm= $form->show($_GET['id']);

?>
<html>
    <head>
        <title>show</title>
    </head>
    <body>
        <h1><b>Registration Form Showing Here</b></h1>
    <dl>
        <dt>Name:</dt>
        <dd><?php echo $frm->name;?></dd>
        <dt>Father:</dt>
        <dd><?php echo $frm->father;?></dd>
        <dt>Mother:</dt>
        <dd><?php echo $frm->mother;?></dd>
        <dt>Email:</dt>
        <dd><?php echo $frm->email;?></dd>
        <dt>Number:</dt>
        <dd><?php echo $frm->number;?></dd>
        <dt>Address:</dt>
        <dd><?php echo $frm->address;?></dd>
    </dl>
    </body>
</html>
